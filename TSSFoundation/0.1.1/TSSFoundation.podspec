#
# Be sure to run `pod lib lint TSSFoundation.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'TSSFoundation'
  s.version          = '0.1.1'
  s.summary          = 'TSSFoundation bring access essential & common functionality for your app.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: The TSSFoundation framework provides a base layer of functionality for apps and frameworks, including serialization, and networking. The classes, protocols defined by TSSFoundation can be used throughout the iOS, watchOS, and tvOS SDKs.
                       DESC

  s.homepage         = 'https://bitbucket.org/rone_loza_gmail_com/tssfoundation'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Rone Loza' => 'rone.loza@gmail.com' }
  s.source           = { :git => 'https://rone_loza_gmail_com@bitbucket.org/rone_loza_gmail_com/tssfoundation.git', :tag => s.version.to_s }
  s.ios.deployment_target = '8.0'
  
  s.default_subspecs = [
  "Core"
  #"Static"
  ]
  
  s.subspec "Core" do |ss|
      
      ss.vendored_frameworks = [
      "TSSFoundation/Frameworks/TSSFoundation-Release-iphoneuniversal/TSSFoundation.framework"
      ]
  end
  
  s.subspec "Serialization" do |ss|
      ss.source_files = 'Framework/TSSFoundation/Serialization/Classes/*.{h,m}'
      ss.public_header_files = 'Framework/TSSFoundation/Serialization/Classes/*.h'
  end
  
  s.subspec "Static" do |ss|
      ss.dependency 'TSSFoundation/Serialization'
  end
  
#s.source_files = 'TSSFoundation/Classes/**/*'
  
  # s.resource_bundles = {
  #   'TSSFoundation' => ['TSSFoundation/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
